{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

-- servant-server
import Servant
-- wai and warp
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp

import Web.Kinto

main :: IO ()
main = do
    initialState <- initialize
    Warp.run 8081 (serve api (serverIO initialState))
