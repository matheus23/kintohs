{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Web.Kinto where

import Prelude hiding (id)

-- lame imports
import qualified Data.Maybe as Maybe
import GHC.Generics
import Control.Concurrent
import Control.Monad.Reader
import Data.Map (Map)
import qualified Data.Map as Map

-- servant-server
import Servant
-- uuid
import Data.UUID (UUID)
import qualified Data.UUID as UUID
import qualified Data.UUID.V4 as UUID
-- time
import Data.Time (UTCTime)
import qualified Data.Time as Time
import qualified Data.Time.Clock.POSIX as Time
-- wai and warp
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
-- aeson
import Data.Aeson

-- API

type API =
         "v1" :> "buckets" :> Get '[JSON] [Bucket]
    :<|> "v1" :> "buckets" :> ReqBody' '[Optional] '[JSON] (Maybe Bucket) :> Post '[JSON] Bucket

api :: Proxy API
api = Proxy

data Bucket = Bucket
    { id :: UUID
    , lastModified :: UTCTime
    }
    deriving (Generic, Show, Eq)

instance FromJSON Bucket where
    parseJSON = withObject "Bucket" $ \o -> Bucket
        <$> o .: "id"
        <*> (fromEpoch <$> o .: "last_modified")

instance ToJSON Bucket where
    toJSON (Bucket id lastModified) =
        object
            [ "id" .= id
            , "last_modified" .= toEpoch lastModified
            ]

fromEpoch :: Int -> UTCTime
fromEpoch epoch = Time.posixSecondsToUTCTime $ (fromIntegral epoch) / 1000

toEpoch :: UTCTime -> Int
toEpoch utcTime = round $ (* 1000) $ Time.utcTimeToPOSIXSeconds utcTime

-- Handling

data AppState
    = AppState
    { buckets :: MVar (Map UUID Bucket)
    }

type AppM = ReaderT AppState Handler

initialize :: IO AppState
initialize = do
    bucketsMVar <- newMVar Map.empty
    return AppState { buckets = bucketsMVar }

applyTransform :: AppState -> AppM a -> Handler a
applyTransform state app = runReaderT app state

generateNewBucket :: IO Bucket
generateNewBucket = do
    id <- UUID.nextRandom
    lastModified <- Time.getCurrentTime
    return Bucket { id = id, lastModified = lastModified }

server :: ServerT API AppM
server = getBuckets
    :<|> postBuckets
    where
        getBuckets :: AppM [Bucket]
        getBuckets = do
            (AppState bucketsMVar) <- ask
            buckets <- liftIO (readMVar bucketsMVar)
            return (Map.elems buckets)
        
        postBuckets :: Maybe Bucket -> AppM Bucket
        postBuckets = \case
            Nothing -> do
                bucket <- liftIO generateNewBucket
                (AppState bucketsMVar) <- ask
                liftIO $ modifyMVar_ bucketsMVar (return . Map.insert (id bucket) bucket)
                return bucket
            Just bucket -> do
                (AppState bucketsMVar) <- ask
                liftIO $ modifyMVar_ bucketsMVar (return . Map.insert (id bucket) bucket)
                return bucket

serverIO :: AppState -> Server API
serverIO appState =
    hoistServer api (applyTransform appState) server
