{-# LANGUAGE LambdaCase #-}

module Main where

-- lame imports
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad
import Data.Function
import Data.Either
import Data.List

-- hspec
import Test.Hspec
-- servant, servant-client, servant-quickcheck
import Servant
import Servant.Client
import Servant.QuickCheck
-- http-client
import Network.HTTP.Client
-- wai, warp
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
-- time
import qualified Data.Time as Time
import qualified Data.Time.Clock.POSIX as Time

import qualified Web.Kinto as Kinto

getBuckets :: ClientM [Kinto.Bucket]
postBuckets :: Maybe Kinto.Bucket -> ClientM Kinto.Bucket
getBuckets :<|> postBuckets = client Kinto.api

main :: IO ()
main = do
    let initializeServer = do
            initialState <- Kinto.initialize
            return (Kinto.serverIO initialState)

    withServantServer Kinto.api initializeServer $ \baseUrl -> do

        -- setup the Client
        manager <- newManager defaultManagerSettings
        let clientEnv = mkClientEnv manager baseUrl
        let runClient clientM = runClientM clientM clientEnv

        hspec $ do
            describe "/buckets GET" $ do
                it "should initially be []" $ do
                    (Right buckets) <- runClient getBuckets
                    buckets `shouldBe` []
            
            describe "/buckets POST" $ do
                it "should answer a single bucket one POST" $ do
                    (Right bucket) <- runClient (postBuckets Nothing)
                    (Right buckets) <- runClient getBuckets
                    buckets `shouldBe` [bucket]

                it "should have last_modified later than before creation" $ do
                    -- the epoch conversation rounds our time to milliseconds!!!
                    currentTime <- Kinto.fromEpoch . Kinto.toEpoch <$> Time.getCurrentTime
                    (Right bucket) <- runClient (postBuckets Nothing)
                    (Kinto.lastModified bucket, currentTime) `shouldSatisfy` uncurry (>=)
                
                it "should not generate buckets with colliding ids" $ do
                    let amountToGenerate = 100

                    results <- replicateM amountToGenerate (runClient (postBuckets Nothing))

                    let buckets = rights results

                    lefts results `shouldBe` []
                    length buckets `shouldBe` amountToGenerate

                    let ids = map Kinto.id buckets

                    nub ids `shouldBe` ids
